#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of the-wrap.
#
# the-wrap is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# the-wrap is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with the-wrap. If not, see <http://www.gnu.org/licenses/>.


sudo apt-get install wget zip unzip make default-jdk

rm -r ./packages/the-wrap-generator-1/
mkdir -p ./packages/the-wrap-generator-1/
cp -r ./the-wrap-generator-1/ ./packages/

cd ./packages/
cd ./the-wrap-generator-1/

printf "Build date: $(date "+%Y-%m-%d").\n" > version.txt
currentDate=$(date "+%Y%m%d")

cd ..
cd ..


cd ./the-wrap-generator-1/
wget https://gitlab.com/publishing-systems/automated_digital_publishing/-/archive/master/automated_digital_publishing-master.zip
unzip automated_digital_publishing-master.zip
mv ./automated_digital_publishing-master/ ./automated_digital_publishing/
cd ./automated_digital_publishing/
make
cd workflows
java setup1
cd ..
cd ..
cd ..

cd ./the-wrap-generator-1/
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
unzip digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
cd workflows
cd setup
cd setup_1
java setup_1
cd ..
cd ..
cd ..
cd ..
cd ..


mkdir -p ./packages/the-wrap-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./the-wrap-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ ./packages/the-wrap-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/

mkdir -p ./packages/the-wrap-generator-1/automated_digital_publishing/html2epub/html2epub2/
cp -r ./the-wrap-generator-1/automated_digital_publishing/html2epub/html2epub2/ ./packages/the-wrap-generator-1/automated_digital_publishing/html2epub/

mkdir -p ./packages/the-wrap-generator-1/automated_digital_publishing/html2epub/html2epub1/workflows/
cp -r ./the-wrap-generator-1/automated_digital_publishing/html2epub/html2epub1/workflows/ ./packages/the-wrap-generator-1/automated_digital_publishing/html2epub/html2epub1/


cd ./packages/

zip -r ./the-wrap-generator-1_$currentDate.zip the-wrap-generator-1
sha256sum ./the-wrap-generator-1_$currentDate.zip > ./the-wrap-generator-1_$currentDate.zip.sha256
rm -r ./the-wrap-generator-1/

cd ..
