#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of the-wrap-generator-1.
#
# the-wrap-generator-1 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# the-wrap-generator-1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with the-wrap-generator-1. If not, see <http://www.gnu.org/licenses/>.

mkdir -p ./temp/
mkdir -p ./source/
mkdir -p ./epub/temp/


java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 jobfile_xml_xslt_transformator_1.xml resultinfo_xml_xslt_transformator_1.xml

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_the-wrap.xml ./source/metadata_the-wrap.xml ./temp/jobfile_html2epub2_the-wrap_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_the-wrap_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/the-wrap.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_office-hour.xml ./source/metadata_office-hour.xml ./temp/jobfile_html2epub2_office-hour_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_office-hour_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/office-hour.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_open-global-mind.xml ./source/metadata_open-global-mind.xml ./temp/jobfile_html2epub2_open-global-mind_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_open-global-mind_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/open-global-mind.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_open-global-mind-soil-and-agriculture.xml ./source/metadata_open-global-mind-soil-and-agriculture.xml ./temp/jobfile_html2epub2_open-global-mind-soil-and-agriculture_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_open-global-mind-soil-and-agriculture_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/open-global-mind-soil-and-agriculture.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_peeragogy-monthly-wrap-working-group-meeting-wrap.xml ./source/metadata_peeragogy-monthly-wrap-working-group-meeting-wrap.xml ./temp/jobfile_html2epub2_peeragogy-monthly-wrap-working-group-meeting-wrap_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_peeragogy-monthly-wrap-working-group-meeting-wrap_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/peeragogy-monthly-wrap-working-group-meeting-wrap.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_togethertec-board-meeting.xml ./source/metadata_togethertec-board-meeting.xml ./temp/jobfile_html2epub2_togethertec-board-meeting_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_togethertec-board-meeting_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/togethertec-board-meeting.epub
rm -rf ./epub/temp/*

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./temp/jobfile_html2epub2_togethertec-saturday-roundtable.xml ./source/metadata_togethertec-saturday-roundtable.xml ./temp/jobfile_html2epub2_togethertec-saturday-roundtable_merged.xml
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./temp/jobfile_html2epub2_togethertec-saturday-roundtable_merged.xml
# TODO: epubcheck
cp ./epub/temp/out.epub ./epub/togethertec-saturday-roundtable.epub
rm -rf ./epub/temp/*
