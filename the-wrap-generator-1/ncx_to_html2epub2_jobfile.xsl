<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2020 Stephan Kreutzer

This file is part of the-wrap-generator-1.

the-wrap-generator-1 is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

the-wrap-generator-1 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with the-wrap-generator-1. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ncx="http://www.daisy.org/z3986/2005/ncx/" exclude-result-prefixes="ncx">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <html2epub2-config>
      <xsl:text>&#xA;</xsl:text>
      <xsl:comment> This file was created by ncx_to_html2epub2_jobfile.xsl of the-wrap-generator-1, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/the-wrap/ and https://publishing-systems.org). </xsl:comment>
      <xsl:text>&#xA;</xsl:text>
      <in>
        <xsl:apply-templates select="./ncx:ncx/ncx:navMap/ncx:navPoint"/>
        <xhtmlSchemaValidation>true</xhtmlSchemaValidation>
        <!-- javax.xml.stream.isValidating: Turns on/off implementation specific DTD validation. -->
        <xhtmlReaderDTDValidation>false</xhtmlReaderDTDValidation>
        <!-- javax.xml.stream.isNamespaceAware: Turns on/off namespace processing for XML 1.0 support. -->
        <xhtmlReaderNamespaceProcessing>true</xhtmlReaderNamespaceProcessing>
        <!-- javax.xml.stream.isCoalescing: Requires the processor to coalesce adjacent character data. -->
        <xhtmlReaderCoalesceAdjacentCharacterData>true</xhtmlReaderCoalesceAdjacentCharacterData>
        <!-- javax.xml.stream.isReplacingEntityReferences: replace internal entity references with their replacement text and report them as characters. -->
        <!-- Always true! -->
        <!-- javax.xml.stream.isSupportingExternalEntities: Resolve external parsed entities. -->
        <xhtmlReaderResolveExternalParsedEntities>true</xhtmlReaderResolveExternalParsedEntities>
        <!-- javax.xml.stream.supportDTD: Use this property to request processors that do not support DTDs. -->
        <xhtmlReaderUseDTDNotDTDFallback>false</xhtmlReaderUseDTDNotDTDFallback>
      </in>
      <out>
        <outDirectory>../epub/temp/</outDirectory>
        <metaData>
          <title>
            <xsl:value-of select="./ncx:ncx/ncx:docTitle/ncx:text/text()"/>
          </title>
          <creator></creator>
          <subject></subject>
          <description></description>
          <publisher></publisher>
          <contributor></contributor>
          <identifier></identifier>
          <source></source>
          <language></language>
          <coverage></coverage>
          <rights></rights>
        </metaData>
      </out>
    </html2epub2-config>
  </xsl:template>

  <xsl:template match="/ncx:ncx/ncx:navMap/ncx:navPoint">
    <inFile title="{./ncx:navLabel/ncx:text/text()}">
      <xsl:text>../source/</xsl:text>
      <xsl:value-of select="./ncx:content/@src"/>
    </inFile>
  </xsl:template>

  <xsl:template match="node()|@*|text()"/>

</xsl:stylesheet>
