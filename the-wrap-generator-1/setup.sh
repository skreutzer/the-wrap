#!/bin/sh
# Copyright (C) 2020 Stephan Kreutzer
#
# This file is part of the-wrap-generator-1.
#
# the-wrap-generator-1 is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# the-wrap-generator-1 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with the-wrap-generator-1. If not, see <http://www.gnu.org/licenses/>.


sudo apt-get install default-jre unzip wget

wget https://gitlab.com/skreutzer/the-wrap-data/-/archive/master/the-wrap-data-master.zip
unzip ./the-wrap-data-master.zip
mv ./the-wrap-data-master/ ./source/
rm ./the-wrap-data-master.zip
